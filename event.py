class Event:
    events_list = []

    def __init__(self, root):
        root.bind('<Key>', self.__bind_key)
        root.protocol('WM_DELETE_WINDOW', self.__delete_window)

    def __delete_window(self):
        self.set('quit', True)

    def __bind_key(self, e):
        self.set('KeyPress', e.keysym)

    def get(self):
        e = self.events_list
        self.events_list = []
        return e

    def set(self, event_type, event_value):
        self.events_list.append({
            'type': event_type,
            'value': event_value
        })
