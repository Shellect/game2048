from data import constants as c


class Deck:
    def __init__(self, canvas):
        self.canvas = canvas

        # Определяем размер 1 ячейки в px
        self.size = (c.SIZE / c.GRID_LEN)  # 100px

        # Создаем список ячеек
        # На старте есть 2 ячейки
        self.cells = [new Cell for i in range(2)]

        # Статусы
        self.compressed = False
        self.moved = False
        self.scores = 0

    def draw(self):
        self.canvas.create_rectangle(
            0, 0, c.SIZE, c.SIZE,
            fill=c.BG_COLOR
        )

        for i in range(c.GRID_LEN):
            for j in range(c.GRID_LEN):
                x = i * self.size + c.GRID_PADDING
                y = j * self.size + c.GRID_PADDING

                self.canvas.create_rectangle(
                    x, y,
                    x + self.size - c.GRID_PADDING * 2,
                    y + self.size - c.GRID_PADDING * 2,
                    fill=c.BG_CELL,
                    width=0  # width of border
                )
