import os

from cell import Cell
from data import constants as c
import numpy
import random


class Grid:
    def __init__(self, canvas):
        self.canvas = canvas
        self.matrix = numpy.empty(shape=(c.GRID_LEN, c.GRID_LEN), dtype=object)

    def add_cell(self):
        x = random.choice(range(c.GRID_LEN))
        y = random.choice(range(c.GRID_LEN))
        while self.matrix[y][x]:
            x = random.choice(range(c.GRID_LEN))
            y = random.choice(range(c.GRID_LEN))
        self.matrix[y, x] = Cell(self.canvas, x, y)

    def check_win(self):
        numpy.any(self.matrix == 2048)

    def has_empty_cells(self):
        numpy.any(self.matrix == 0)

    def can_merge(self):
        return numpy.any(self.matrix[0:, 1:] == self.matrix[0:, :-1]) \
               or numpy.any(self.matrix[1:, 0:] == self.matrix[:-1, 0:])

    def left_merge(self):
        for y, x in numpy.argwhere(self.matrix[0:, 1:] == self.matrix[0:, :-1]):
            if self.matrix[y, x]:
                self.matrix[y, x] *= 2
                self.matrix[y, x + 1] = None

    def left_compress(self):
        new_matrix = []
        for i in self.matrix:
            new_matrix.append(sorted(i, key=bool, reverse=True))
        self.matrix = numpy.array(new_matrix)

    def move(self, direction):
        old_matrix = numpy.array(self.matrix)
        if direction == 'Up':
            self.up()
        elif direction == 'Down':
            self.down()
        elif direction == 'Left':
            self.left()
        elif direction == 'Right':
            self.right()
        return not numpy.all(self.matrix == old_matrix)

    def up(self):
        self.matrix = self.matrix.T
        self.left()
        self.matrix = self.matrix.T

    def left(self):
        self.left_compress()
        self.left_merge()
        self.left_compress()

    def down(self):
        self.matrix = self.matrix.T
        self.right()
        self.matrix = self.matrix.T

    def right(self):
        self.matrix = numpy.fliplr(self.matrix)
        self.left()
        self.matrix = numpy.fliplr(self.matrix)

    def print(self):
        if os.name == 'nt':
            os.system('cls')
        else:
            os.system('clear')

        print(self.matrix)

        for y, x in numpy.argwhere(self.matrix):
            self.matrix[y, x].column = x
            self.matrix[y, x].row = y
            self.matrix[y, x].move()
