from data import constants as c
import random
import math


class Cell:
    def __init__(self, canvas, column, row):
        self.canvas = canvas
        self.size = (c.SIZE / c.GRID_LEN)  # 100
        self.column = column
        self.row = row
        self.x = self.get_x()
        self.y = self.get_y()

        self.number = 2 + 2 * (random.random() > .9)

        self.square = canvas.create_rectangle(
            self.x,
            self.y,
            self.x + self.size,
            self.y + self.size,
            fill=c.COLORS_BG[self.number],
            width=c.GRID_PADDING,
            outline=c.BG_COLOR
        )

        self.text = canvas.create_text(
            self.get_text_x(),
            self.get_text_y(),
            fill=c.COLORS_FG[self.number],
            font=c.FONT,
            text=str(self.number),
            width=self.size,
        )

    def __imul__(self, n):
        self.set_number(self.number * n)
        return self

    def __nonzero__(self):
        return bool(self.number)

    def __repr__(self):
        return str(self.number)

    def __str__(self):
        return str(self.number)

    def __ne__(self, other):
        return self.number != other

    def __eq__(self, other):
        if isinstance(other, Cell):
            return self.number == other.number
        else:
            return False

    def __del__(self):
        self.canvas.delete(self.square)
        self.canvas.delete(self.text)

    def set_number(self, number):
        self.number = number
        self.canvas.itemconfig(self.square, fill=c.COLORS_BG[number])
        font_size = 40 - 8 * math.floor(math.log10(number))
        self.canvas.itemconfig(
            self.text,
            fill=c.COLORS_FG[number],
            text=str(number),
            font=("Verdana", font_size, "bold")
        )

    def get_x(self):
        return self.column * self.size

    def get_y(self):
        return self.row * self.size

    def get_text_x(self):
        return self.column * self.size + self.size / 2

    def get_text_y(self):
        return self.row * self.size + self.size / 2

    def move(self):
        delta_x = self.get_x() - self.x
        delta_y = self.get_y() - self.y

        distance_x = delta_x / abs(delta_x) * self.size if delta_x  else 0
        distance_y = delta_y / abs(delta_y) * self.size if delta_y  else 0
        self.canvas.move(
            self.square,
            distance_x,
            distance_y
        )
        self.canvas.move(
            self.text,
            distance_x,
            distance_y
        )

        self.x = self.canvas.coords(self.square)[0]
        self.y = self.canvas.coords(self.square)[1]
