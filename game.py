from board import Board
from data import constants as c
from tkinter import *
from event import Event
from clock import Clock
from grid import Grid


def create_canvas():
    root = Tk()
    root.title('2048')

    ws = root.winfo_screenwidth()  # width of the screen
    hs = root.winfo_screenheight()  # height of the screen

    x = (ws / 2) - (c.SIZE / 2)  # x position of left top window corner
    y = (hs / 2) - (c.SIZE / 2)  # y position of left top window corner

    root.geometry("%dx%d+%d+%d" % (c.SIZE, c.SIZE, x, y))
    root.configure(background=c.BG_COLOR)
    root.resizable(False, False)
    # root.iconbitmap("favicon.ico")

    canvas = Canvas(root, width=c.SIZE, height=c.SIZE, bg=c.BG_COLOR)
    canvas.pack()

    return root, canvas


class Game:
    def __init__(self):
        self.start_cells = 2
        self.game_over = True
        self.win = False
        self.root, self.canvas = create_canvas()
        self.event = Event(self.root)
        self.clock = Clock(c.FPS)
        self.board = Board(self.canvas)
        self.board.draw()
        self.grid = Grid(self.canvas)

    def start(self):
        for i in range(self.start_cells):
            self.grid.add_cell()

        self.key_handler()
        self.mainloop()

    def can_move(self):
        return self.grid.has_empty_cells() or self.grid.can_merge()

    def check_status(self):
        if self.grid.check_win():
            pass
        elif not self.can_move():
            pass

    def key_handler(self):
        for e in self.event.get():
            if e['type'] == 'quit':
                self.game_over = False
            if e['type'] == 'KeyPress':
                if self.grid.move(e['value']):
                    self.grid.add_cell()

    def mainloop(self):
        while self.game_over:
            self.clock.tick()
            self.key_handler()
            self.grid.print()
            self.check_status()
            self.root.update()
        self.root.quit()
