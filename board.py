from data import constants as c


class Board:
    def __init__(self, canvas):
        self.size = (c.SIZE / c.GRID_LEN)
        self.canvas = canvas

    def draw(self):
        for i in range(c.GRID_LEN):
            for j in range(c.GRID_LEN):
                x = i * self.size
                y = j * self.size

                self.canvas.create_rectangle(
                    x, y,
                    x + self.size,
                    y + self.size,
                    fill=c.BG_CELL,
                    width=c.GRID_PADDING,
                    outline=c.BG_COLOR
                )
